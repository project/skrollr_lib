Download skrollr lib from https://github.com/Prinzhorn/skrollr and place
in /libraries/skrollr

The skrollr lib will need to be attached to the render array of any block, node,
or entity that you want the library included on.

example:

/*
 * Implements hook_block_view
 */
function hook_block_view_alter(&$data, $block) {
  if ($block->delta == 'block_delta' || $block->delta == 'other_block_delta') {
    $data['content']['#attached']['libraries_load'][] = array('skrollr');
    $data['content']['#attached']['js'][] = drupal_get_path('module', 'skrollr_lib') . '/js/skrollr_lib.js';
  }
}

The skrollr_lib.js file only initializes the skrollr lib. There are no options
for the skrollr initialization.

If it is desired to have this effect a block then this snipit might be usefull:

/**
 * Implements theme_preprocess_block().
 */
function hook_preprocess_block(&$vars) {
  $block = $vars['block'];
  $skrollr_attributes = array(
    'block_id',
  );

  $skrollr_attributes['block_id']['data-0'] = array(
    'margin-bottom:0%',
  );
  $skrollr_attributes['block_id']['data-900'] = array(
    'margin-bottom:-10%',
  );

  if ($block->delta == 'block_id') {
    $vars['attributes_array'] = array_merge($vars['attributes_array'], $skrollr_attributes['block_id]);
  }
}
